# README #
### What is this repository for? ###

A webapp will be launched in docker-containers, that logs to logstash.
Gradle will be used as well as Spring boot for the webapp.
The Example-Environment is setup with Vagrant, docker and packer.

* vmimage

A prepared image to start a Vagrant-VM.
This will be used to spawn the containers in.
Provisioning will be done with a shell-script (setup.sh).
Maybe I'll use puppet in some future version, just to test...

* webapp

A simple webapp, that provides some REST-services.
This application has a logger configured to log into logstash.

* elk

The configuration files for elk.
input from webapp, codec for the specific logging and output to elasticsearch.


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
